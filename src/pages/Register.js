import { useState, useEffect, useContext } from "react"
import { Form, Button, Modal } from "react-bootstrap"
import { Navigate, useNavigate } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2"

export default function Register() {

    const navigate = useNavigate();

    const { user, setUser } = useContext(UserContext);
    const [firstName, setFirstName] = useState("")
    const [lastName, setLastName] = useState("")
    const [mobileNo, setMobileNo] = useState("")
    const [email, setEmail] = useState("")
    const [password1, setPassword1] = useState("")
    const [password2, setPassword2] = useState("")
    const [isActive, setIsActive] = useState(false)


    function registerUser(e) {

        e.preventDefault()

        fetch("http://localhost:4000/users/checkEmail", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    email: email
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                if (data === true) { //meaning email is not unique

                    Swal.fire({
                        title: "Duplicate email found",
                        icon: "error",
                        text: "Please provide a different email."
                    })

                } else {

                	fetch("http://localhost:4000/users/register", {
                		method: "POST",
                		headers: {
                			"Content-Type": "application/json"
                		},
                		body: JSON.stringify({
                			firstName: firstName,
                			lastName: lastName,
                			email: email,
                			mobileNo: mobileNo,
                			password: password1
                		})
                	})

                    setFirstName("")
                    setLastName("")
                    setMobileNo("")
                    setEmail("")
                    setPassword1("")
                    setPassword2("")

                    Swal.fire({
                        title: "Registration successful",
                        icon: "success",
                        text: "Welcome to Zuitt!"
                    })

                    navigate("/login")
                }

            })
    }

    useEffect(() => {
    	//no blank fields
    	//mobile number must be exactly 11 characters
    	//passwords must match

        if (
            (firstName !== "" &&
                lastName !== "" &&
                mobileNo.length === 11 &&
                email !== "" &&
                password1 !== "" &&
                password2 !== "") &&
            (password1 === password2)
        ) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [firstName, lastName, mobileNo, email, password1, password2])

    return (
        (user.id !== null)
         ?
        <Navigate to ="/courses" />
         :
        <Form className="mt-3" onSubmit = {(e) => registerUser(e)}>
      <Form.Group className="mb-3" controlId="firstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control 
            type="text" 
            placeholder="First Name"
            value = {firstName}
            onChange = {e => setFirstName(e.target.value)}
            required />        
      </Form.Group>

      <Form.Group className="mb-3" controlId="lastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control 
            type="text" 
            placeholder="Last Name"
            value = {lastName}
            onChange = {e => setLastName(e.target.value)}
            required />        
      </Form.Group>

      <Form.Group className="mb-3" controlId="mobileNo">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control 
            type="text" 
            placeholder="Enter mobile number"
            value = {mobileNo}
            onChange = {e => setMobileNo(e.target.value)}
            required />        
      </Form.Group>

      <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Email Address</Form.Label>
        <Form.Control 
            type="email" 
            placeholder="Enter email"
            value = {email}
            onChange = {e => setEmail(e.target.value)}
            required />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control 
            type="password" 
            placeholder="Verify Password" 
            value={password1}
            onChange={ e => setPassword1(e.target.value)}
            required />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password2">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control 
            type="password" 
            placeholder="Verify Password" 
            value={password2}
            onChange={ e => setPassword2(e.target.value)}
            required />
      </Form.Group>
      
      {
        isActive ? 
          <Button variant="primary" type="submit" id="submitBtn">
            Register
          </Button>
          :
          <Button variant="danger" type="submit" id="submitBtn" disabled>
            Register
          </Button>
      }

    </Form>
    )
}