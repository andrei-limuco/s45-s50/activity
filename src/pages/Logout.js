import { Navigate } from "react-router-dom";
import { useContext, useEffect } from "react";
import UserContext from "../UserContext";

export default function Logout() {
	const { unsetUser, setUser } = useContext(UserContext);
	//localStorage.clear() method will allow us to clear the information in the Local Storage
	// localStorage.clear();
	unsetUser();

	useEffect(() => {
		setUser({id: null});
	}, []);

	return <Navigate to="/login" />;
}
