// import { Fragment } from "react";
// import { useState, useEffect } from "react";
import { Card, Button } from "react-bootstrap";
import {Link} from "react-router-dom"

export default function CourseCard({ courseProp }) {
	const { name, description, price, _id} = courseProp;
	// console.log(courseProp)

	// Syntax:
	// const [getter, setter] = useState(initalGetterValue)
	// const [count, setCount] = useState(0);
	// const [seats, setSeats] = useState(30);

	// function enroll() {
	// 	setCount(count + 1);
	// 	console.log("Enrollees: " + count);
	// 	setSeats(seats - 1);
	// 	console.log("Seats: " + seats);
	// }

	// useEffect(() => {
	// 	if (seats === 0) {
	// 		alert("No more seats available.");
	// 	}
	// }, [seats]);

	return (
		<Card>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>Php {price}</Card.Text>
				<Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
			</Card.Body>
		</Card>
	);
}

// export default function CourseCard(props) {
// 	return (
// 		<Fragment>
// 			<Card>
// 				<Card.Body>
// 					<Card.Title>{props.name}</Card.Title>
// 					<Card.Subtitle>Description:</Card.Subtitle>
// 					<Card.Text>{props.description}</Card.Text>
// 					<Card.Subtitle>Price:</Card.Subtitle>
// 					<Card.Text>{props.price}</Card.Text>
// 					<Button variant="primary">Enroll</Button>
// 				</Card.Body>
// 			</Card>
// 		</Fragment>
// 	);
// }
